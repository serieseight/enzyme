$(function(){

var $window = $(window),
    $documnet = $(document),
    $wrapper = $('#wrapper'),
    i = 0,
    pageheaderZoneLinkk = $('.ph-zone-link'),
    dropdownMenus = $('.dropdown'),
    dropdownMenus2 = $('.dropdown-2');


pageheaderZoneLinkk.on('click', function(e){
  var $this = $(this),
      $thisSibling = $this.next('.dropdown');


  if ($thisSibling.hasClass('active')){
    $thisSibling.removeClass('active');
    $this.removeClass('active');
  } else {
    dropdownMenus.removeClass('active');
    dropdownMenus2.removeClass('active');
    $thisSibling.addClass('active');
    $this.addClass('active');
  }

  e.stopPropagation();
  e.preventDefault();
});

dropdownMenus.on('click', function(e){
  e.stopPropagation();
});

dropdownMenus2.on('click', function(e){
  e.stopPropagation();
});


var searchForm = $('#search-form'),
    searchFormInput = searchForm.children('input');


searchForm.on('click', function(e){
  searchFormInput.addClass('active');

  e.stopPropagation();
});

// Carousel
// ===================================
var carousel = $('#carousel'),
    carouselSlides = carousel.children('.slide').length;

carousel.owlCarousel({
  items: 1,
  mouseDrag: false,
  touchDrag: false,
  nav: true,
  navText: ['<i class="icon icon-arrow-left" id="prev-slide"></i>','<i class="icon icon-arrow-right" id="next-slide"></i']
});

var currentSlide = 1,
    nextSlide = $('#next-slide'),
    prevSlide = $('#prev-slide'),
    numberOfSlides = $('#number-of-slides');

numberOfSlides.text('1 of ' + carouselSlides);

nextSlide.on('click', function(){
  if (currentSlide < carouselSlides ){
    currentSlide++;
    numberOfSlides.text(currentSlide + ' of ' + carouselSlides);
  }
});

prevSlide.on('click', function(){
  if (currentSlide > 1 ){
    currentSlide--;
    numberOfSlides.text(currentSlide + ' of ' + carouselSlides);
  }
});

var notf = $('#notf');

notf.on('click', function(e){
  var $thisSibling = $(this).next('.dropdown-2');

  if ($thisSibling.hasClass('active')){
    $thisSibling.removeClass('active');
  } else {
    $thisSibling.addClass('active');
    dropdownMenus.removeClass('active');
  }

  e.stopPropagation();
  e.preventDefault();
});


// Popup
// ===================================
var overlay = $('#overlay'),
    closePopup = $('.icon-close'),
    cancelPopup = $('.cancel-popup'),
    submitPopupForm = $('.submit-popup'),
    userProfilePopup = $('#user-profile-popup'),
    userProfilePopupTrigger = $('#user-profile-popup-trigger'),
    cardPopup = $('#card-popup'),
    cardPopupTrigger = $('.card'),
    createHazard = $('#create-hazard'),
    createHazardTrigger = $('.create-hazard'),
    updateHazard = $('#update-hazard'),
    updateHazardTrigger = $('.update-hazard'),
    createHazardSituation = $('#create-hazard-situation'),
    createHazardSituationTrigger = $('.create-hazard-situation'),
    updateHazardSituation = $('#update-hazard-situation'),
    updateHazardSituationTrigger = $('.update-hazard-situation'),
    createHarm = $('#create-harm'),
    createHarmTrigger = $('.create-harm'),
    updateHarm = $('#update-harm'),
    updateHarmTrigger = $('.update-harm');

// Show popup
function showPopup(popup, triggerButton){
  this.$popup = $(popup);
  this.$triggerButton = $(triggerButton);
}

showPopup.prototype.init = function(){
  var $popup = this.$popup;

  this.$triggerButton.on('click', function(e){
    var $wrapperHeight = $wrapper.height();


    if ($popup.selector === '#card-popup'){
      checkTags(tagsCollect);
    } else if ($popup.selector === '#create-harm'){
      checkTags(fieldControls);
    } else if ($popup.selector === '#update-harm'){
      checkTags(fieldControls2);
    }

    if (!tracingModOn){
      overlay.addClass('active');
      overlay.addClass('front');
      overlay.height($wrapperHeight);
      $popup.addClass('active');

      checkPopupHieght($popup);
    }

    e.stopPropagation();
    e.preventDefault();
  });

};

new showPopup(userProfilePopup, userProfilePopupTrigger).init();
new showPopup(cardPopup, cardPopupTrigger).init();
new showPopup(createHazard, createHazardTrigger).init();
new showPopup(updateHazard, updateHazardTrigger).init();
new showPopup(createHazardSituation, createHazardSituationTrigger).init();
new showPopup(updateHazardSituation, updateHazardSituationTrigger).init();
new showPopup(createHarm, createHarmTrigger).init();
new showPopup(updateHarm, updateHarmTrigger).init();

// Submit form
submitPopupForm.on('click', function(){
  var $thisForm = $(this).parents('.popup-footer').prev('.inner-space').children('form');

  $thisForm.submit();
});

// Hide popup
function hidePopup(button){
  this.$button = $(button);
}

hidePopup.prototype.init = function(){
  var $thisPopup = this.$button.parents('.popup-body'),
      $button = this.$button;

  this.$button.on('click', function(){
    $thisSibling = $button.data('sibling');

    if (overlay.css('padding-bottom') !== '0px'){
      overlay.css('padding-bottom', '0');
    }

    if ($thisSibling === 'yes'){
      $thisPopup.removeClass('active');
      docInfoTabs.addClass('active');
      docPageMain.height('auto');
    } else {
      // $wrapper.removeClass('hide');
      overlay.removeClass('active');
      $thisPopup.removeClass('active');

      setTimeout(function(){
        overlay.removeClass('front');
      }, 250);
    }
  });
};

for(i=0;i<closePopup.length;i++){
  new hidePopup(closePopup[i]).init();
}

for(i=0;i<cancelPopup.length;i++){
  new hidePopup(cancelPopup[i]).init();
}


function checkPopupHieght(){
  var $windowHeight = $window.height(),
      activePopup = $('.popup-body.active'),
      $popupHeight = activePopup.height()+170;

  if (overlay.css('padding-bottom') !== '0px'){
    overlay.css('padding-bottom', '0');
  }

  if($popupHeight > $windowHeight){
    overlay.css('padding-bottom', activePopup.height()/3 + 'px');
  }
}


var chboxPlusSibling = $('.chbox-plus-sibling');

chboxPlusSibling.on('click', function(){
  setTimeout(checkPopupHieght, 250);
});


// Tracing
// ===================================
var mainColumn1 = $('.main-column').eq(0).children('.main-column-inner'),
    mainChBox = $('#checkbox-1'),
    tracingModOn = false;

$('<div class="overlay-2" id="overlay-2"></div').appendTo(mainColumn1);

mainChBox.on('click', function() {
  var $this = $(this);
      overlay2 = $('#overlay-2');

  if( $this.is(':checked')){
    chechTrace();
    tracingModOn = true;
    tracingOn();
  } else {
    cardPopupTrigger.removeClass('tracing-green tracing-red');
    tracingModOn = false;
    tracingOff();
  }
});

function chechTrace(){
  for(i=0;i<cardPopupTrigger.length;i++){
    var $this = $(cardPopupTrigger[i]),
        $thisTrace = $this.data('trace');

    if ($thisTrace === 'green'){
      $this.addClass('tracing-green');
    } else {
      $this.addClass('tracing-red');
    }
  }
}

function tracingOn(){

  cardPopupTrigger.on('mouseenter', function(){
    var $this = $(this),
        $thisId = $this.data('trace-id');

    if(!isNaN($thisId)){
      getMySiblings($thisId);
      overlay2.addClass('active front');
    }

    // $this.addClass('tracing-card');

  });

  cardPopupTrigger.on('mouseleave', function(){
    var $this = $(this);

    cardPopupTrigger.removeClass('tracing-card');
    overlay2.removeClass('active');

    $('.trace-canvas').remove();
    overlay2.removeClass('front');
  });
}

function tracingOff(){
  cardPopupTrigger.off('mouseenter');
  cardPopupTrigger.off('mouseleave');
}


function getMySiblings(id){
  var mySiblings = [];

  for(i=0;i<cardPopupTrigger.length;i++){
    var $this = $(cardPopupTrigger[i]),
        $thisTraceId = $this.data('trace-id');

    if ($thisTraceId === id){
      mySiblings.push($this);
      $this.addClass('tracing-card');
    }
  }

  createCanvas(mySiblings);
}

function createCanvas(array){
      prevHeight = 0;
      prevPositionTop = 0;
      prevPositionLeft = 0;

  for(i=0;i<array.length;i++){
    var $this = $(array[i]),
        $thisWidth = $this.outerWidth(),
        $thisHieght = $this.outerHeight(),
        $thisPositionTop = $this.position().top,
        $thisPositionLeft = $this.position().left,
        lineDirection = 0,
        lineVal = 0,
        $thisType = $this.data('trace');


    if (i){

      cssWidth = $thisPositionLeft - (prevPositionLeft+$thisWidth);

      if ($thisPositionTop >= prevPositionTop){
        cssHieght = ($thisPositionTop+$thisHieght) - prevPositionTop;
        lineDirection = 'down';
      } else {
        cssHieght = (prevPositionTop+prevHeight) - $thisPositionTop;
        lineDirection = 'up';
      }

      cssLeft = prevPositionLeft+$thisWidth+10;

      if ($thisPositionTop >= prevPositionTop){
        cssTop = prevPositionTop;
      } else {
        cssTop = $thisPositionTop;
      }

      canvas = '<canvas class="trace-canvas" width="'+ cssWidth +'" height="'+ cssHieght +'" data-direction="'+ lineDirection +'" data-dirc-val="'+ $thisHieght +'" data-trace="'+ $thisType +'""></canvas>';
      $canvas = $(canvas);
      $canvas.appendTo(mainColumn1);

      $canvas.css({
        top: cssTop,
        left: cssLeft
      });

    }
    prevHeight = $thisHieght;
    prevPositionTop = $thisPositionTop;
    prevPositionLeft = $thisPositionLeft;

  }

  drawLines();
}

function drawLines(){
  var traceCanvas = document.getElementsByClassName('trace-canvas');

  for(i=0;i<traceCanvas.length;i++){
    var thisItem = traceCanvas[i],
        $traceCanvas = $(thisItem),
        $traceCanvasDirection = $traceCanvas.data('direction'),
        $traceCanvasDirVal = $traceCanvas.data('dirc-val'),
        $traceCanvasWidth = $traceCanvas.width(),
        $traceCanvasHeight = $traceCanvas.height(),
        $traceType = $traceCanvas.data('trace'),
        lineColor = '#55cd9a',
        startPointY = 0,
        endPointY = 0;

    var c = thisItem.getContext('2d');

    if ($traceCanvasDirection === 'down'){
      startPointY = 50;
      endPointY = $traceCanvasDirVal - 50;
      endPointY = $traceCanvasHeight-endPointY;
    } else {
      startPointY = prevPositionTop+50;
      endPointY = 50;
      startPointY = $traceCanvasHeight - (($traceCanvasHeight/100)*30);
    }

    if ($traceType === 'red'){
      lineColor = '#ffa19d';
    }

    c.beginPath();
    c.moveTo(-1,startPointY);
    c.quadraticCurveTo($traceCanvasWidth/2,$traceCanvasWidth/2,$traceCanvasWidth+1,endPointY);
    c.lineWidth = 2;
    c.strokeStyle = lineColor;
    c.stroke();
  }

}

// Card
// ===================================
var cardInfo = $('#card-info'),
    cardInfoForm = cardInfo.children('form'),
    cardInfoContent = cardInfo.children('.content'),
    cardInfoBtnEdit = cardInfoContent.children('.btn');

cardInfoBtnEdit.on('click', function(e){
  cardInfoForm.removeClass('d-none');
  cardInfoContent.addClass('d-none');

  e.preventDefault();
});

// Animated input fields
// ===================================
var inputTitleFields = $('.anm-input').children('input'),
    inputTitleFieldsLables = inputTitleFields.prev('label');


function checkInputTitleFieldsValues(){
  for (i=0;i<inputTitleFields.length;i++){
    var $this = $(inputTitleFields[i]),
        $thisValue = $this.val(),
        $thisLabel = $this.prev('label');

    if($thisValue !== ''){
      $thisLabel.addClass('active');
    } else if($thisLabel.hasClass('active')) {
      $thisLabel.removeClass('active');
    }
  }
}

checkInputTitleFieldsValues();

inputTitleFields.on('focus', function(){
  var $this = $(this),
      $thisLabel = $this.prev('label');

  $thisLabel.addClass('active');
});

inputTitleFields.on('focusout', function(){
  checkInputTitleFieldsValues();
});


// Accordion Menu
// ===================================
var accordionMenu = $('#accordion-menu'),
    accordionMenuList = accordionMenu.children('li'),
    accordionMenuActiveLink = accordionMenu.find('.active-link'),
    accordionMenuMainLinks = accordionMenuList.children('.link');

accordionMenuActiveLink.parents('li').addClass('active');

accordionMenuMainLinks.on('click', function(e){
  var $this = $(this),
      $thisMenu = $this.siblings('ul'),
      $thisParent = $this.parent('li'),
      $thisParentSiblings = $thisParent.siblings('li'),
      otherMenus = $thisParentSiblings.children('ul');

  $thisMenu.slideToggle(200);
  otherMenus.slideUp(200);

  if ($thisParent.hasClass('active')){
    $thisParent.removeClass('active');
  } else {
    $thisParent.addClass('active');
    $thisParentSiblings.removeClass('active');
  }

  e.preventDefault();
});


var accordionMenuListActive = accordionMenu.children('li.active');
accordionMenuListActive.children('ul').slideDown(200);


// Tabs
// ===================================
var tabsNavLinks = $('.tabs-nav').children('a'),
    tabsContent = $('.tabs-content'),
    tabFirstHeight = $('#tab-1').height(),
    tabsBody = $('.tabs-body'),
    tabsBodyChild = $('.tabs-body-child'),
    expHistory = $('#exp-history');

function tabs(elem, children){
  this.$elem = $(elem);
  this.children = children;
}

tabs.prototype.init = function(){
  var $thisTabsNav = this.$elem.children('.tabs-nav'),
      $thisLinks = $thisTabsNav.children('a'),
      $thisContent = this.$elem.children('.tabs-content'),
      $thisContentTabActive = $thisContent.children('.active').height(),
      haveTabParent = this.children;

  $thisContent.height($thisContentTabActive);

  if (haveTabParent){
    var $thisParentContent = this.$elem.parents('.tabs-content');
  }

  $thisLinks.on('click', function(e){
    var $this = $(this),
        $thisId = $this.data('tab'),
        $thisTab = $thisContent.children('.tab-'+$thisId),
        $thisTabHeight = $thisTab.height();

    $thisContent.addClass('of-hidden');

    $this.addClass('active').siblings('a').removeClass('active');
    $thisTab.addClass('active').siblings('.tab').removeClass('active');
    $thisContent.height($thisTabHeight);

    if ($thisTabsNav.hasClass('outer-link')){
      if ($this.hasClass('history')){
        expHistory.addClass('active');
      } else {
        expHistory.removeClass('active');
      }
    }

    if (haveTabParent){
      checkParentTabHeight($thisParentContent);
    }

    setTimeout(function(){
      $thisContent.removeClass('of-hidden');
    },250);

    e.preventDefault();
  });
};

for(i=0;i<tabsBodyChild.length;i++){
  new tabs(tabsBodyChild[i], true).init();
}
new tabs(tabsBody, false).init();

function checkParentTabHeight(parent){

  setTimeout(function(){
    var activeTab = parent.children('.active').height();
    parent.height(activeTab);

  },240);

}


// Tiny dropdown menus
// ===================================
var tinyDropdowns = $('.tiny-dropdown'),
    btnDropdown = $('.btn-dropdown'),
    tinyForm = $('.tiny-form'),
    tinyDropdownsOpts = $('.tiny-dropdown-opts').children('a');

btnDropdown.on('click', function(e){
  var $this = $(this),
      $thisDropdown = $this.next('.tiny-dropdown');

  if ($thisDropdown.hasClass('active')){
    $thisDropdown.removeClass('active');
  } else {
    tinyDropdowns.removeClass('active');
    $thisDropdown.addClass('active');
  }

  if ($this.hasClass('bold')){
    var $thisGrandparent = $this.parents('.table-3-body');

    if (!$thisGrandparent.hasClass('expanded')){
      var $thisGrandparentRows = $thisGrandparent.find('.table-3-row');

      for (i=0;i<$thisGrandparentRows.length;i++){

        if (i>=1){
          $($thisGrandparentRows[i]).addClass('d-none');
        }
      }
      $thisGrandparent.addClass('of-visible');
    } else {
      $thisGrandparent.addClass('of-visible');
    }

  }

  e.stopPropagation();
  e.preventDefault();
});

tinyForm.on('click', function(e){
  e.stopPropagation();
});

tinyDropdownsOpts.on('click', function(e){
  var $this = $(this),
      $thisGrandparent = $this.parents('.table-3-body'),
      $thisGrandparentRows = $thisGrandparent.find('.table-3-row'),
      $thisTxt = $this.text(),
      $thisFieldVal = $this.parent('.tiny-dropdown').prev('a');

  $thisFieldVal.text($thisTxt);

  $thisGrandparentRows.removeClass('d-none');
  $thisGrandparent.removeClass('of-visible');
  e.preventDefault();
});

// Options List
// ===================================
var optsListLinks = $('.opts-list').find('a'),
    fieldControls = $('#controls-field'),
    fieldControls2 = $('#controls-field-2');

optsListLinks.on('click', function(e){
  var $this = $(this),
      $thisValue = $this.text(),
      $thisParent = $this.parent('li'),
      $thisGrandparent = $this.parents('.opts-list-wrap'),
      $thisInput = $thisGrandparent.children('input'),
      $thisInputFront = $thisGrandparent.children('.btn-option').children('span');

  if ($thisGrandparent.hasClass('multi-options')){
    var $thisInputValue = $thisInput.val();
    $('<span class="tag doc-tag">'+ $thisValue +'<i class="icon icon-close"></i></span>').appendTo($thisGrandparent);

    $thisInput.val($thisInputValue + ' ' + $thisValue);

    checkTags($thisInput);
  } else if ($thisGrandparent.hasClass('dd-menu')){
    var $thisParentMenu = $thisGrandparent.children('.tiny-dropdown');

    $thisParent.addClass('active').siblings('li').removeClass('active');
    $thisInput.val($thisValue);
    $thisInputFront.text($thisValue);
    $thisParentMenu.removeClass('active');
  } else {
    $thisParent.addClass('active').siblings('li').removeClass('active');
    $thisInput.val($thisValue);
    $thisInputFront.text($thisValue);
  }

  e.preventDefault();
});


// Add new document
// ===================================
var addNewDoc = $('#add-new-doc'),
    docPageMain = $('#doc-page-main'),
    docInfoTabs = $('#doc-info-tabs'),
    newDocForm = $('#new-doc-form');

addNewDoc.on('click', function(e){

  docInfoTabs.removeClass('active');
  newDocForm.addClass('active');

  docPageMain.height(490);

  e.preventDefault();
});

// Upload new Doc
// ===================================
var uploadNewDoc = $('#upload-new-doc'),
    uploadNewDocInput = uploadNewDoc.prev('input'),
    uploadNewDocName = uploadNewDocInput.prev('p');

uploadNewDoc.on('click', function(){
  var $this = $(this);

  uploadNewDocInput.trigger('click');
});

uploadNewDocInput.on('change', function(){
  var $thisVal = uploadNewDocInput.val(),
      $thisValStartName = $thisVal.lastIndexOf('\\'),
      $thisValClean = $thisVal.slice($thisValStartName+1);
  if ($thisVal !== ''){
    uploadNewDocName.removeClass('d-none');
    uploadNewDocName.children('i').text($thisValClean);
  } else {
    uploadNewDocName.addClass('d-none');
  }
});

// 2
var uploadFileForm = $('#upload-file-form'),
    uploadFileFormPart1 = uploadFileForm.children('.part-1'),
    uploadFileFormPart2 = uploadFileForm.children('.part-2'),
    uploadNewDoc2 = $('#upload-new-doc-2'),
    uploadNewDocInput2 = uploadNewDoc2.prev('input'),
    progressBar = $('.progress');


uploadNewDoc2.on('click', function(){
  var $this = $(this);

  uploadNewDocInput2.trigger('click');
});

uploadNewDocInput2.on('change', function(e){

  var data = new FormData(uploadFileForm[0]);

  $.ajax({
      url: 'upload.php',
      type: 'POST',
      data: data,
      cache: false,
      processData: false,
      contentType: false,
      xhr: function() {
          var myXhr = $.ajaxSettings.xhr();

          uploadFileFormPart1.addClass('d-none');
          uploadFileFormPart2.removeClass('d-none');

          if(myXhr.upload){
              myXhr.upload.addEventListener('progress',checkProgress);
          }
          return myXhr;
      },
      success: function(data, textStatus, jqXHR){

        uploadFileFormPart1.removeClass('d-none');
        uploadFileFormPart2.addClass('d-none');

      },
      error: function(jqXHR, textStatus, errorThrown){

        uploadFileFormPart1.children('.notf').removeClass('d-none');

      }
  });

});

function checkProgress(e){
  var totalSize = e.total,
			currentProgress = e.loaded,
			procent = totalSize/100,
			current = currentProgress/procent;

	progressBar.width(current+'%');
}


// Tags
// ==================================
var tags = $('#tags'),
    tagsCollect = $('#tags-collect'),
    dependencyTags = $('#dependency-tags'),
    dependencyTagsCollect = $('#dependency-tags-collect');

function typeTags(typeField, valuesField, tagsStyle){
  this.$typeField = typeField;
  this.$valuesField = valuesField;
  this.$tagsStyle = tagsStyle;
}

typeTags.prototype.init = function(){
  var valuesField = this.$valuesField,
      tagsStyle = this.$tagsStyle;

  if (tagsStyle === 1){
    tagsStyle = 'max';
  }

  this.$typeField.on('keyup', function(e){
   var $this = $(this);

   if (e.keyCode == 13) {
     var $thisVal = $this.val(),
         valuesFieldVal = valuesField.val();

     if ($thisVal !== ''){
       $('<span class="tag doc-tag '+ tagsStyle +'">'+ $thisVal +'<i class="icon icon-close"></i></span>').insertBefore(valuesField);

       $this.val('');
       valuesField.val(valuesFieldVal + ' ' + $thisVal);
       checkTags(valuesField);
     }
   }

  });

};

new typeTags(tags, tagsCollect, '').init();
new typeTags(dependencyTags, dependencyTagsCollect, 1).init();



function checkTags(mainField){
  var docTagsClose = $('.doc-tag').children('.icon');

  docTagsClose.off('click');
  docTagsClose.on('click', function(){
    var $thisParent = $(this).parent('span'),
        $thisParentText = $thisParent.text(),
        $thisParentTextLen = $thisParentText.length,
        fieldTagsVal = mainField.val(),
        $thisValIndex = fieldTagsVal.indexOf($thisParentText),
        part1Val = fieldTagsVal.slice(0, $thisValIndex),
        part2Val = fieldTagsVal.slice($thisValIndex+$thisParentTextLen+1);

    mainField.val(part1Val+part2Val);
    $thisParent.remove();
  });
}

// Table 3
// ===================================
var tables3 = $('.table-3-body'),
    table3rows = $('.table-3-row'),
    expandTable = $('.expand-row');

for(i=0;i<tables3.length;i++){
  var $this = $(tables3[i]),
      $thisExpand = $this.children('.expand-row'),
      $thisChildNum = $this.find('.table-3-row').length;

  if($thisChildNum>1){
    $thisExpand.addClass('d-block');
  }
}

expandTable.on('click', function(){
  var $this = $(this),
      $thisDirection = $this.data('direction'),
      $thisIcon1 = $this.children('.icon-1'),
      $thisIcon2 = $this.children('.icon-2'),
      $thisTable = $this.parent('.table-3-body'),
      $thisTableInnerHeight = $this.prev('.table-3-inner').outerHeight();

  if ($thisDirection === 'up'){
    $thisTable.height($thisTableInnerHeight);
    $thisIcon1.addClass('d-none');
    $thisIcon2.removeClass('d-none');
    $thisTable.addClass('expanded');
    $this.data('direction','down');
  } else {
    $thisTable.height(62);
    $thisIcon1.removeClass('d-none');
    $thisIcon2.addClass('d-none');
    $thisTable.removeClass('expanded');
    $this.data('direction','up');
  }

});


// Lines
// ===================================
var lines = $('.line');

for(i=0;i<lines.length;i++){
  var $this = $(lines[i]),
      $thisParentHeight = $this.parent('.change').outerHeight();

  $this.height($thisParentHeight);
}

// Datepicker
// ===================================
$('#date').datepicker();


// Document
// ===================================
$documnet.on('click', function(){
  dropdownMenus.removeClass('active');
  dropdownMenus2.removeClass('active');
  pageheaderZoneLinkk.removeClass('active');
  searchFormInput.removeClass('active');
  tinyDropdowns.removeClass('active');
  tables3.removeClass('of-visible');
  table3rows.removeClass('d-none');
});

});
