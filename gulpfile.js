var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer');

var source = 'source/';

gulp.task('sass', function() {
  return gulp.src(source+'sass/main.scss')
    .pipe(sass({
      outputStyle: 'compressed'
    })
    .on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 5 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('concat', function(){
  return gulp.src([
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/owl.carousel/dist/owl.carousel.min.js',
    'bower_components/jquery-ui-custom/jquery-ui.min.js',
    source + 'js/custom.js'
  ])
  .pipe(concat('main.js'))
  .pipe(uglify())
  .pipe(gulp.dest('js'));
});

gulp.task('default', ['sass', 'concat'], function() {
  gulp.watch(source+'sass/*.scss', ['sass']);
  gulp.watch(source+'js/*.js', ['concat']);
});
